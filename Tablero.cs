﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Final
{
    public abstract class Tablero
    {

        public string Tipo { get; set; }
        public int Codigo { get; set; }
        public double Largo { get; set; }
        public double Ancho { get; set; }
        public int EdadMinima { get; set; }
        public int EdadMaxima { get; set; }
        public decimal PrecioBase { get; set; }



        public virtual string TablerosDisponibles()
        {
            return $"Tipo: {Tipo} |Codigo: {Codigo} |Tamaño: {Largo} x {Ancho} ";
         
        }

        public abstract int CalcularFechaEntrega();
    }
}
