﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Final
{
    public class Principal
    {
        List<Cliente> Clientes = new List<Cliente>();

        List<Pedido> Pedidos = new List<Pedido>();

        //Corrección: En este método no hay que usar un parámetro por referencia, hay que armar una clase con ambas propiedades (una bool y otra string) y devolver esa clase.
        public bool RegistrarNuevoCliente ( Cliente clientes, ref string mensaje, string apellido)
        {
            Cliente nuevaCliente = new Cliente();
            
            foreach(var cliente in Clientes)
            {
                if (clientes.DNI == cliente.DNI)
                {
                    mensaje = "Esta incorrecto";
                    return false;
                }                            
            }
            //Corrección: Para validar si NumeroTelefono es null la propiedad deberia ser int?, sino la validacion debe ser con su valor por defecto que es 0
            if (clientes.Nombre == null && clientes.Domicilio == null && clientes.NumeroTelefono == null)
            {
                mensaje = "Esta incorrecto";
                return false;
            }
            nuevaCliente = clientes;
            Clientes.Add(nuevaCliente);
            return true;                 
        }


        public  int RegistrarNuevoPedido (int clienteDNI, int codigoProducto)
        {
            Pedido nuevoPedido = new Pedido();

            nuevoPedido.DNICliente = clienteDNI;
            nuevoPedido.NumeroPedido = Pedidos.Count() + 1;
            nuevoPedido.FechaCreacionPedido = DateTime.Today;
            nuevoPedido.FechaEntregaEstimada = DateTime.Today.AddDays();

            var edad = null;


            
            //Incompleto.

        }

        //Corrección: Falta el método para retornar el listado
        //Corrección: Falta el método para actualizar el estado de un pedido
    }
}
