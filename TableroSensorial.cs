﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Final
{
    public class TableroSensorial: Tablero
    {
        public int CantidadDeJuegos { get; set; }
        public string ColorTablero { get; set; }
        public bool DiseñoPersonalizado { get; set; }


        public override string TablerosDisponibles()
        {
            return base.TablerosDisponibles() + $"|Cantidad de juegos: {CantidadDeJuegos} |Es personalizado: {DiseñoPersonalizado}";
        }

        public override int CalcularFechaEntrega()
        {
            if(DiseñoPersonalizado == true)
            {
                return 14;
            }
            return 10;
        }
    }
}
