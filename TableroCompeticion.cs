﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Final
{
    public class TableroCompeticion: Tablero
    {
        public string Color { get; set; }
        
        public Categorias Categoria { get; set; } 

        public int MaximoJugadores { get; set; }
        public enum Categorias
        {
            Deporte, Didactico, Estrategia
        }
        public override string TablerosDisponibles()
        {
            return base.TablerosDisponibles() + $"|Cantidad de Jugadores: {MaximoJugadores} |Categoria: {Categoria}";

        }

        public override int CalcularFechaEntrega()
        {
            if (Categoria == Categorias.Estrategia)
            {
                return 10;
            }
            return 15;
        }



    }
}
