﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Final
{
    public class Pedido
    {
        public int NumeroPedido { get; set; }
        public int DNICliente { get; set; }
        public DateTime FechaCreacionPedido { get; set; }
        public DateTime FechaEntregaEstimada { get; set; }
        public decimal CostoTotalVenta { get; set; }
        public Formasdepago FormaDePago { get; set; }

        public DateTime FechaDePago { get; set; }

        public enum Formasdepago
        {
            Efectivo, Transferencia, Mercadopago
        }

    }
}
