﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Final
{
    public class Cliente
    {
        public int DNI { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int CodigoPostal { get; set; }
        public string Domicilio { get; set; }
        public string CorreoElectronico { get; set; }
        public int NumeroTelefono { get; set; }
    }
}
