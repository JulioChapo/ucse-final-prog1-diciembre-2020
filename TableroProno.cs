﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica_Final
{
    public class TableroProno: Tablero
    {
        public int CantidadBarras { get; set; }
        public bool DiseñoPersonalizado { get; set; }
        public int Colores { get; set; }

        //Corrección: borrar código innecesario
        public 

        public override string TablerosDisponibles()
        {
            return base.TablerosDisponibles() + $" Cantidad de barras: {CantidadBarras} | Es personalizado: {DiseñoPersonalizado}";
        }

        public override int CalcularFechaEntrega()
        {
            if (CantidadBarras >= 3)
            {
                if (Colores == 3)
                {
                    return 12;
                }

                return 10;
            }
            else if (Colores == 3)
            {
                return 17;
            }
            return 15;

        }

    }
}
